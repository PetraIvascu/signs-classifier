"""

data_utils.py contains functions to be used for preprocessing, augmenting the
data

"""
from __future__ import absolute_import

import tensorflow as tf
from configs.config_train_scratch import training_params


def read_images(filename):
  image_string = tf.read_file(filename)
  image = tf.image.decode_jpeg(
      image_string, channels=training_params['num_channels'])
  image = tf.image.convert_image_dtype(image, tf.float32)
  return image


def crop(image, image_size, resize_size, is_training=True):
  if is_training:
    image = tf.random_crop(image, [image_size, image_size, 3])
    image = tf.image.random_flip_left_right(image)
  else:
    crop_min = tf.abs(resize_size / 2 - (image_size / 2))
    crop_max = crop_min + image_size
    image = image[crop_min:crop_max, crop_min:crop_max, :]
  return image


def resize(image, image_size):
  resized_image = tf.image.resize_images(image, (image_size, image_size))
  return resized_image


def augmentations(image, types_augmentations):
  if types_augmentations['random_flip_left_right']:
    image = tf.image.random_flip_left_right(image)
  if types_augmentations['random_brightness']:
    image = tf.image.random_brightness(image, max_delta=32.0 / 255.0)
  if types_augmentations['random_saturation']:
    image = tf.image.random_saturation(image, lower=0.5, upper=1.5)
  # make sure image is still part of [0,1] --> normalized
  image = tf.clip_by_value(image, 0.0, 1.0)
  return image
