"""

architecture.py contains a pre-define very simple CNN used for classification in
case transfer learning is not desired

"""

from __future__ import absolute_import

import tensorflow as tf


def get_basic_architecture(features, num_classes):
  """
    defines a basic CNN used for classification by model.py

    Arguments:
      features {tf.float32} -- images preprocessed ready to be consumed by model
      num_classes {int} -- number of classes used for the dense layer

    Returns:
      tf.float32 -- logits from last layer of architecture
  """

  net = features

  net = tf.layers.conv2d(
      inputs=net,
      name='layer_conv1',
      filters=16,
      kernel_size=5,
      padding='same',
      activation=tf.nn.relu)
  net = tf.layers.max_pooling2d(inputs=net, pool_size=2, strides=2)

  # Second convolutional layer.
  net = tf.layers.conv2d(
      inputs=net,
      name='layer_conv2',
      filters=36,
      kernel_size=5,
      padding='same',
      activation=tf.nn.relu)
  net = tf.layers.max_pooling2d(inputs=net, pool_size=2, strides=2)

  # Flatten to a 2-rank tensor.
  net = tf.layers.flatten(net)
  # Eventually this should be replaced with:
  # net = tf.layers.flatten(net)

  # First fully-connected / dense layer.
  # This uses the ReLU activation function.
  net = tf.layers.dense(
      inputs=net, name='layer_fc1', units=128, activation=tf.nn.relu)

  # Second fully-connected / dense layer.
  # This is the last layer so it does not use an activation function.
  net = tf.layers.dense(inputs=net, name='layer_fc2', units=num_classes)
  return net
