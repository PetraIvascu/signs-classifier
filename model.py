"""
  model.py creates the model_fn and input_fn need it by estimators
  model_fn --> gets the architecture or does the transfer learning;
               structured into train, eval, predict
  input_fn --> gets the dataset read from TFRecords and stores them
               into a dictionary

"""

from __future__ import absolute_import

import os
import tensorflow as tf
import tensorflow_hub as hub

from dataloader import ReadTFRecords
from architecture import get_basic_architecture


class Model():
  """
    defines 2 important methods for working with estimators: model_fn, input_fn
  """

  def __init__(self, config, batch_size):
    """
    initializes the model class with batch_size and the config_file

    Arguments:
      config {dictionary: key(string) value(int, boolean, string)} -- contains
      all the hyperparameters and arguments given as input from user
      batch_size {int} -- the dimention of the batch used for training
    """

    self.image_size = config['size']
    self.num_classes = config['num_classes']
    self.batch_size = batch_size
    self.read_tf_records = ReadTFRecords(self.image_size, self.batch_size,
                                         self.num_classes)
    self.config = config

  def input_fn(self, file_pattern, training):
    """
    input function given to the estimator for train, eval or predict

    Arguments:
      file_pattern {string} -- pattern similar with train*.tfrecords used to
      know where to read records from
      training {boolean} -- True is training / False not training
    Returns:
      [dictionaries] -- 1st dict contains the images
                        2nd dict contains the label one-hot-encoded and the
                        original label
    """

    dataset = self.read_tf_records(file_pattern, self.config, training)
    features = {'image': dataset[0]['image']}
    labels = {
        'class_vec': dataset[1]['class_vec'],
        'class_idx': dataset[1]['class_idx']
    }

    return features, labels

  def model_fn(self, features, labels, mode, params):
    """
    model function taken by the estimator for train, eval or predict responsible
    for consumming the features and labels
    model_fn does:
      - transfer learning acordingly to the initial model from config file
      - trains from scratch using a pre-defined achitecture
      - training/evaluation/prediction

    Arguments:
      features {tf.float32} -- the images batched
      labels {tf.float32} -- the labels one-hot-encoded
      mode {tf.estimator} -- the mode (train/eval/predict)

    Returns:
      object - Estimator -- TrainSpec/EvalSpec/PredictSpec
    """
    training = mode == tf.estimator.ModeKeys.TRAIN
    if params['fine_tune_ckpt']:
      print("Fine tuning network ...")
      # Load ResNet model
      module = hub.Module(
          params['module_spec'], trainable=False, name=params['module_name'])
      outputs = module(features['image'])
      logits = tf.layers.dense(inputs=outputs, units=self.num_classes)
    else:
      print("Training network from scratch ...")
      logits = get_basic_architecture(features['image'], self.num_classes)

    if mode == tf.estimator.ModeKeys.PREDICT:
      _, top_5 = tf.nn.top_k(logits, k=5)
      predictions = {
          'top_1': tf.argmax(logits, -1),
          'top_5': top_5,
          'probabilities': tf.nn.softmax(logits),
          'logits': logits,
      }
      return tf.estimator.EstimatorSpec(mode, predictions=predictions)

    loss = tf.losses.softmax_cross_entropy(labels["class_vec"], logits)
    # create histogram of class spread
    tf.summary.histogram("classes", labels["class_idx"])

    if training:
      optimizer = tf.train.AdamOptimizer(learning_rate=params["learning_rate"])
      train_op = optimizer.minimize(loss, tf.train.get_global_step())

      if params["output_train_images"]:
        tf.summary.image("training", features["image"])

      return tf.estimator.EstimatorSpec(mode, loss=loss, train_op=train_op)

    if mode == tf.estimator.ModeKeys.EVAL:
      # define the metrics:
      metrics_dict = {
          'Accuracy':
          tf.metrics.accuracy(tf.argmax(logits, axis=-1), labels["class_idx"])
      }
      # output eval images
      eval_summary_hook = tf.train.SummarySaverHook(
          save_steps=100,
          output_dir=os.path.join(params["model_dir"], "eval"),
          summary_op=tf.summary.image("validation", features["image"]))

      # return eval spec
      return tf.estimator.EstimatorSpec(
          mode,
          loss=loss,
          eval_metric_ops=metrics_dict,
          evaluation_hooks=[eval_summary_hook])
