"""
config_train_scratch.py config file containig hyperparameters

"""

from __future__ import absolute_import

training_params = {
    "num_classes": 6,
    "size": 64,
    "num_channels": 3,
    # the base learning rate used in the polynomial decay
    "base_lr": 0.4,
    # how many steps to warmup the learning rate for
    "warmup_iter": 780,
    "learning_rate": 1e-6,
    # What learning rate to start with in the warmup phase
    # (ramps up to base_lr)
    "warmup_start_lr": 0.1,
    "total_steps": 180,
    # transfer learning architecture
    "module_spec":
    'https://tfhub.dev/google/imagenet/resnet_v2_50/feature_vector/1',
    "module_name": 'resnet_v2_50',
    # augmentations
    "augment": False,
    "random_flip_left_right": True,
    "random_brightness": True,
    "reandom_saturation": True
}
