"""
Custom init
"""
import os
import sys
import re
import importlib


def load_plugins():
  pysearchre = re.compile('.py$', re.IGNORECASE)
  pluginfiles = filter(pysearchre.search, os.listdir(os.path.dirname(__file__)))
  form_module = lambda fp: '.' + os.path.splitext(fp)[0]
  plugins = map(form_module, pluginfiles)
  # import parent module / namespace
  importlib.import_module('configs')
  configs = {}
  for plugin in plugins:
    if not plugin.startswith('.__'):
      print(plugin)
      configs[plugin[1:]] = importlib.import_module(
          plugin, package="configs").training_params

  return configs


configs = load_plugins()
