# Signs-Classifier

#### -- Project Status: On-Hold

## Project Intro/Objective
The purpose of this project is to showcase how to work with estimators from 
Tensorflow.



### Methods Used
* transfer learning using resnet_v2_50
* basic CNN 
* estimators
* tf records


### Technologies
 
* Python
* Tensorflow
* Tensorflow Estimators API
* Tensorflow Dataset API
* Tensorflow Hub


## Project Description
This project exists only because I wanted to preserve my larning curve regarding 
TF Estimators, TF Dataset, TF Hub with a focus on coding standards.



