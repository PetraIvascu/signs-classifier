"""

dataloader.py used for loading dataset from already existing TFRecords

"""
from __future__ import absolute_import

import random
import multiprocessing
import tensorflow as tf

from utils.data_utils import resize, augmentations, crop


def _parse_function(example_proto,
                    image_size,
                    num_classes,
                    config,
                    training,
                    mean_value=(123, 117, 104),
                    method="resize"):
  """
  Parsing function decodes, processes, augments tfrecord entry

  _parse_function does:
    - extracts data
    - resizes or crops image
    - encodes label
    - augments dataset

  Arguments:
    example_proto {tf record entry} -- tfrecord entry
    image_size {int} -- resizing dimension
    num_classes {int} -- number of classes
    training {boolean} -- True if training / False if not training
    mean_values {tuple} -- mean to subtrac from image; default - (123, 117, 104)
    method {string} -- keyword indicating if resize/crop image;
                       default - 'resize'

  Returns:
    dictionary -- [description]
  """

  # Schema of fields to parse
  schema = {
      'filename': tf.FixedLenFeature([], tf.string),
      'rows': tf.FixedLenFeature([], tf.int64),
      'cols': tf.FixedLenFeature([], tf.int64),
      'channels': tf.FixedLenFeature([], tf.int64),
      'image': tf.FixedLenFeature([], tf.string),
      'label': tf.FixedLenFeature([], tf.int64)
  }

  # Extract the data record

  image_size = tf.cast(image_size, tf.int32)
  mean_value = tf.cast(tf.stack(mean_value), tf.float32)

  # Parse example using schema
  parsed_features = tf.parse_single_example(example_proto, schema)
  jpeg_image = parsed_features["image"]
  # generate correctly sized image using one of 2 methods
  if method == "crop":
    image = crop(jpeg_image, image_size, image_size, is_training=training)
  elif method == "resize":
    image = tf.image.decode_image(jpeg_image)
    img_shape = tf.stack([
        parsed_features['rows'], parsed_features['cols'],
        parsed_features['channels']
    ])
    image = tf.reshape(image, img_shape)
    image = resize(image, image_size)
  else:
    raise "unknown image process method"

  if config['augment'] is True and training is True and random.uniform(
      0.0, 1.0) > 0.5:
    # do augmentations
    image = augmentations(image, types_augmentations=config)

  # subtract mean
  image = image - mean_value

  # subtract 1 from class index as background class 0 is not used
  label_idx = tf.cast(parsed_features['label'], dtype=tf.int32) - 1

  # create one hot vector
  label_vec = tf.one_hot(label_idx, num_classes)

  return {
      "image": tf.reshape(image, [image_size, image_size, 3])
  }, {
      "class_idx": label_idx,
      "class_vec": label_vec
  }


class ReadTFRecords():
  """
  deals with the reading of tfrecords from their files

  """

  def __init__(self, image_size, batch_size, num_classes):
    self.image_size = image_size
    self.batch_size = batch_size
    self.num_classes = num_classes

  def __call__(self, glob_pattern, config, training=True):
    """ Read tf records matching a glob pattern

        Arguments:
            glob_pattern {string} --
            eg. "/usr/local/share/Datasets/Imagenet/train-*.tfrecords"

        Keyword Arguments:
            training {bool} -- Whether or not to shuffle the data for
            training and evaluation (default: {True})
        """
    threads = multiprocessing.cpu_count()
    with tf.name_scope("tf_record_reader"):
      # generate file list
      files = tf.data.Dataset.list_files(glob_pattern, shuffle=training)
      dataset = files.apply(
          tf.contrib.data.parallel_interleave(
              lambda filename: tf.data.TFRecordDataset(filename),
              cycle_length=threads))

      dataset = dataset.map(
          map_func=lambda example: _parse_function(
              example,
              self.image_size,
              self.num_classes,
              config,
              training=training),
          num_parallel_calls=threads)
      dataset = dataset.shuffle(buffer_size=1080)
      dataset = dataset.repeat()
      dataset = dataset.batch(batch_size=self.batch_size)

      # prefetch batch
      dataset = dataset.prefetch(buffer_size=2)

      return dataset.make_one_shot_iterator().get_next()


if __name__ == "__main__":
  # used for testing purposes
  global_pattern = 'C:/Users/ivasc/Documents/datasets/signs/*.tfrecords'
  read_tf_records = ReadTFRecords(64, 4, 6)
  results = read_tf_records(global_pattern)
  print(results[0]['image'].get_shape())
  print(results[1]['class_vec'].get_shape())
  print(results[1]['class_idx'].get_shape())
